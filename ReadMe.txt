# FishTracking Task

write a small program that will automatically or (semi-automatically) detect and track the fish in the tank and output x,y coordinates for each frame
use any suitable and preferable programming language or library (Matlab, Python, C/C++, OpenCV etc.)

there are 5 video files provided in the directory, use one on random for development and the other for testing

# extra points:

- solution does not need to be perfect, aim for clean and understandable code, as well as to provide a minimal working solution

- provide all necessary means for running the program + adequate annotation and user instructions

- avoid 3rd party ready-to-use code or programs

- output fish coordinates or leave a trail behind

- detect the arena and output time spent in black vs. light background

- automatic fish detection (no user interaction to point the fish)

- differentiate between fish head and tail

- log a progress, feature and bug report

# deadline 10-14 days
send solutions to georgi.tushev@brain.mpg.de
