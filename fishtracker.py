# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 18:40:23 2015

@author: steven leigh

prerequisites:
Python 2.7.6
NumPy 1.8.2
OpenCV 2.4.8 
This was tested in IDE Sypder 2.2.5 on Ubuntu 14.04
All of these can be installed on ubuntu with just these two commands:
sudo apt-get install sypder
sudo pip install python-opencv

General Description:
A toy problem to demonstrate scientific programming skills relating to computer 
vision.  This script reads one of five videos of a fish in a tank.  The main 
goal of this script is to extract and track the location of the fish.  The 
basic approach taken is to first use histogram matching to identify pixels
likely belonging to the fish.  Then a particle filter is used to incorporate
basic fish dynamics with the noisy observations of the fish location.

Usage:
When the scirpt is run one of the fish videos appears.  The user can pause 
the video by pressing spacebar.  The user can click on the fish in the video
at any point to assign it to the tracker.  The tracker is black/white
dependent so the user will have to select the fish at least once when it
is on the black half and once on the white half.  The estimated location of
the fish is identified by the red ellipse.  If the ellipse strays too far
from the fish the user can re-centre the tracking by again clicking on the
fish.  The user can also cancel detection for one half of the arena by right
clicking on that half.

Data Overlay:
blue: estimated arena
red: estimated fish location and angle.  Line from centre of ellipse points 
towards estimated location of fish tail.
green: sample of particles from particle filter
"""

import numpy as np
import cv2

# Input video
cap = cv2.VideoCapture('Video02_09Nov2015.mp4')

# Global state
video_paused = False  # Current state of video playback
raw_frame = None  # Current video frame
fish_hist_black = None  # Histogram of fish on black side
fish_hist_white = None  # Histogram of fish on white side
arena_coords = None  # Coordinates of arena
frames_black = 0  # Count the frames the fish was on black, white, or unknown
frames_white = 0
frames_unknown = 0
user_clicks = 0  # Track how many times user input was required

# Particle filter
PARTICLE_COUNT = 800
fish_particles = np.ones((6,PARTICLE_COUNT))*-200  # 100 particles tracking x,y coordinates and x,y speeds
particle_weights = np.ones((1,PARTICLE_COUNT))

# Detection algorithm tuning parameters
HIST_CHANNELS = [0,2]
HIST_BINS = [64, 64]
HIST_RANGES = [0,180,0,256]

FILTER_ANGLES = [0,45,90,135]  # List of angles for the filter bank


def calcHist(x, y):
    """Calculate the histogram of the fish at point (x,y)  
    
    Store result in global variable fish_hist
    """
    global raw_frame
    global fish_hist_black    
    global fish_hist_white
    global arena_coords
    
    #TODO: do the arena limits help at all here?
    (arena_top, arena_bottom, arena_left, arena_right, arena_centre_row) = arena_coords
    arena_frame = raw_frame[arena_top:arena_bottom, arena_left:arena_right, :]
    HSV = cv2.cvtColor(arena_frame, cv2.COLOR_BGR2HSV)
    mask = np.zeros([np.size(HSV,0), np.size(HSV,1)], np.uint8)
    cv2.circle(mask, (x-arena_left,y-arena_top), 10, 1, -1)
    fish_hist = cv2.calcHist([HSV], HIST_CHANNELS, mask, HIST_BINS, HIST_RANGES)
    if x<arena_centre_row:  # update the fish histogram depending on the side of the arena the fish is on
        fish_hist_black = fish_hist
    else:
        fish_hist_white = fish_hist
    #fish_hist = cv2.calcHist([HSV], [2], mask, [256], [0,256])   
    

def selectFish(event, x, y, flags, param):
    """Event triggered when user has mouse actions on video.
    
    Either updates a fish histogram with a left click, or clears a fish 
    histogram withe a right click.
    """
    global video_paused       
    global arean_coords
    global fish_hist_black
    global fish_hist_white
    global user_clicks
    
    if event == cv2.EVENT_LBUTTONUP:
        calcHist(x,y)
        #update all fish particles to mouse click location
        for idx in range(np.size(fish_particles,1)):
            p = fish_particles[:,idx]
            p[0] = x
            p[1] = y
        video_paused = False
        user_clicks += 1
    elif event == cv2.EVENT_RBUTTONUP:  # Clear fish histogram by right clicking
        centre_x = arena_coords[4]
        if x<centre_x:
            fish_hist_black = None
        else:
            fish_hist_white = None
        video_paused = False
        user_clicks += 1
        
    
def detectFish(frame, fish_hist_black, fish_hist_white, arena_coords):
    """Detect the location of the fish.
    
    Returns and angle invariant filter response map,  and a set of angle 
    dependent filter response maps.
    Returns (None, None) if no fish histograms are calculated.
    """
    if fish_hist_black is None and fish_hist_white is None:
        return (None, None)
    
    # Limit fish detection area to arena and black/white side dependent
    (arena_top, arena_bottom, arena_left, arena_right, arena_centre_row) = arena_coords
    # Arena_frame = frame[arena_top:arena_bottom, arena_left:arena_right, :]
    black_frame = frame[arena_top:arena_bottom, arena_left:arena_centre_row, :]
    white_frame = frame[arena_top:arena_bottom, arena_centre_row:arena_right, :]
    
    if fish_hist_black is None:
        black_project = black_frame[:,:,0]/256
    else:
        # Apply histogram backprojection to black frame
        HSV = cv2.cvtColor(black_frame, cv2.COLOR_BGR2HSV)
        black_project = cv2.calcBackProject([HSV], HIST_CHANNELS, fish_hist_black, HIST_RANGES, 1)
        black_project = np.divide(np.array(black_project,np.float), np.max(black_project))  # normalize to [0,1]       
    
    if fish_hist_white is None:
        white_project = white_frame[:,:,0]/256
    else:
        # Apply histogram backprojection to white frame
        HSV = cv2.cvtColor(white_frame, cv2.COLOR_BGR2HSV)
        white_project = cv2.calcBackProject([HSV], HIST_CHANNELS, fish_hist_white, HIST_RANGES, 1)
        white_project = np.divide(np.array(white_project,np.float), np.max(white_project))  # Normalize to [0,1]       
    
    recombined_frame = np.append(black_project, white_project,1)
    
    # Apply fish shaped filter at various angles
    filtered = np.zeros([np.size(recombined_frame,0), np.size(recombined_frame,1), len(FILTER_ANGLES)])
    for idx, ellipse_angle in enumerate(FILTER_ANGLES):
        kernel = np.zeros([67,67],np.uint8)
        cv2.ellipse(kernel, center=(33,33), axes=(33, 11), angle=ellipse_angle, startAngle=0, endAngle = 360, color=1, thickness=-1)  # thickness=-1 means filled
        filtered[:,:,idx] = cv2.filter2D(recombined_frame, -1, kernel)

        # Show filter responses for debugging        
        #cv2.namedWindow('fish hist filter angled: ' + str(ellipse_angle),0)
        #cv2.resizeWindow('fish hist filter angled: ' + str(ellipse_angle),600,400)        
        #cv2.imshow('fish hist filter angled: ' + str(ellipse_angle), filtered[:,:,idx]/450)
    
    filt_norm = np.max(filtered,2) * (1.0/np.max(filtered))
    cv2.namedWindow('hist response',0)
    cv2.resizeWindow('hist response',600,400)    
    cv2.imshow('hist response', filt_norm)
    
    return (np.max(filtered,2), filtered)

    
def detectArena(frame):
    """Detect the location of the arena.
    
    Assume rectangular arena with vertical division in centre.
    Returns coordinates of arena: (top, bottom, left, right, centre row)
    """
    # Theshold value band to identify pixels of black half of arena
    HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    val = HSV[:,:,2]
    val = val<75
    val = val*1.0
    #cv2.imshow('arena debug 1', val)
    
    # Convolve a large filter roughly size of black half to find centre
    kernel = np.zeros([600,600])
    cv2.rectangle(kernel, (100,100), (500,500), 1, -1)
    dst = cv2.filter2D(val, -1, kernel)
    dst *= 1.0/(np.max(dst)+0.001)
    #cv2.imshow('arena debug 2', dst)
    
    # Find highest signal
    idx = np.argmax(dst)
    centre_black_x = idx%np.size(dst,1)  # Watch (x,y) vs [row,col] mapping
    centre_black_y = np.int64(np.floor(idx/np.size(dst,1)))
    
    # Hard code arena dimensions
    top = centre_black_y - 185
    bottom = centre_black_y + 185
    left = centre_black_x - 160
    right = centre_black_x + 530
    centre_row = centre_black_x + 190
    return (top, bottom, left, right, centre_row)


def resampleWeights():
    """Remove unlikely particles and replace with likely ones.
    
    Do this via importance sampling.
    """
    global particle_weights
    global fish_particles
    particle_weights *= 1/np.sum(particle_weights)
    bins = np.cumsum(particle_weights,1)  # Bin size is proportional to particle weight
    
    indexes=[]
    for n in range(PARTICLE_COUNT):
        uni_samp = np.random.uniform()  # Take a sample from a uniform distribution
        for idx in range(PARTICLE_COUNT):
            if uni_samp<bins[0,idx]:  # See which bin it lands in
                indexes.append(idx)  # That particle is selected for the new set of particles
                break
    
    # Update particle set with new distribution of samples
    new_particles = np.copy(fish_particles)
    for idx in range(PARTICLE_COUNT):
        new_particles[:,idx] = fish_particles[:,indexes[idx]]
    fish_particles = new_particles
    particle_weights = np.ones((1,PARTICLE_COUNT))
    

def trackFish(observe_map, arena_coords, angle_map):
    """Track the fish using a particle filter
    """
    global fish_particles
    global particle_weights
    
    # Move particles according to fish model
    # Current fish model is basic conservation of momentum with random accelerations and random heading purterbations
    for idx in range(PARTICLE_COUNT):
        p = fish_particles[:,idx]        
        p[2] = p[2] + np.random.normal(0,50)  # x speed
        p[3] = p[3] + np.random.normal(0,50)  # y speed
        p[0] = p[0] + p[2]  # x position
        p[1] = p[1] + p[3]  # y position
        if np.linalg.norm([p[2],p[3]]) > 40:  # Fish can drift slowly sideways, but never moves quickly sideways or backwards
            p[4] = p[2]
            p[5] = p[3]
            p[4] = p[4] / np.linalg.norm([p[4],p[5]])  #Normalize heading to unit vector
            p[5] = p[5] / np.linalg.norm([p[4],p[5]])
        p[4] =  p[4] + np.random.normal(0,0.2)  # Heading vector x
        p[5] =  p[5] + np.random.normal(0,0.2)  # Heading vector y
        p[4] = p[4] / np.linalg.norm([p[4],p[5]])  # Normalize heading to unit vector
        p[5] = p[5] / np.linalg.norm([p[4],p[5]])
        
    # Update particle weights based on how well angle filter response predicts fish postion and angle
    (arena_top, arena_bottom, arena_left, arena_right, arena_centre_row) = arena_coords
    if angle_map is not None:
        for idx in range(PARTICLE_COUNT):
            # Find particle's closest angle in FILTER_ANGLES
            best_angle_idx = 0
            best_angle_match = -1
            for idx_angle, angle in enumerate(FILTER_ANGLES):
                n1 = np.linalg.norm([p[4] + np.cos(np.degrees(angle)), p[5] - np.sin(np.degrees(angle))])  # Compare two directions because the angle filters are 180 degrees symmetric
                n2 = np.linalg.norm([-p[4] + np.cos(np.degrees(angle)), -p[5] - np.sin(np.degrees(angle))])
                max_n = np.max([n1, n2])
                if max_n>best_angle_match:
                    best_angle_idx = idx_angle
                    best_angle_match = max_n
                    
            fish_x = fish_particles[0,idx]            
            fish_y = fish_particles[1,idx]
            if fish_x<arena_left or fish_x>arena_right or fish_y>arena_bottom or fish_y<arena_top:  # Set to zero weight if fish particle went out of arena
                weight = 0
            else: 
                weight = angle_map[fish_y-arena_top, fish_x-arena_left, best_angle_idx]  # set weight as the filter response of the closest angle at the particle location
            particle_weights[0,idx] *= weight
                
    # Compute expected fish state
    particle_weights *= 1.0/np.sum(particle_weights,1)
    fish_state = np.dot(fish_particles, particle_weights[0,:].T)  # Compute the expectation of each state variable
    fish_state[4] = fish_state[4] / np.linalg.norm([fish_state[4], fish_state[5]])  # Normalize heading to unit vector
    fish_state[5] = fish_state[5] / np.linalg.norm([fish_state[4], fish_state[5]])  
        
    if np.size(particle_weights,1)/2.0 > 1.0/np.dot(particle_weights,particle_weights.T):
        resampleWeights()
    
    return fish_state
    
    
def drawParticles(frame):
    """Draw the particles from the particle filter on a frame for debugging purposes
    """
    for idx in range(PARTICLE_COUNT/2):  # Only show half the particles because it gets cluttered
        x=int(fish_particles[0,idx])
        y=int(fish_particles[1,idx])
        w=int(particle_weights[0,idx])  # TODO: somehow show the weight of each particle?
        cv2.circle(frame, (x,y), 1, [0,255,0],-1, lineType=cv2.CV_AA)
    
    
def drawFish(frame, fish_state):
    """Draw a symbol around the fish.
    
    An ellipse to indicate the angle of the fish and a line towards one end
    to mark the tail direction.
    """
    (fish_x, fish_y, fish_vx, fish_vy, heading_x, heading_y) = fish_state
    fish_angle = np.degrees(np.arctan2(-heading_y, -heading_x))
    cv2.ellipse(frame, (int(fish_x),int(fish_y)), (33,11), fish_angle, startAngle=0, endAngle=360, color=[0,0,255], thickness=1, lineType=cv2.CV_AA)  #thickness=-1 means filled
    cv2.line(frame, (int(fish_x),int(fish_y)), (int(heading_x*(-50) + fish_x), int(heading_y*(-50) + fish_y)), [0,0,255])    

    
def annotateFrame(frame, fish_state, arean_coords):
    """Return a copy of the frame annotated with info
    """
    global frames_black
    global frames_white
    global frames_unknown
    global frame_count
    global user_clicks 
    
    (fish_x, fish_y, fish_vx, fish_vy, heading_x, heading_y) = fish_state
    (arena_top, arena_bottom, arena_left, arena_right, arena_centre_row) = arena_coords
    annotate_frame = np.copy(frame)
    cv2.rectangle(annotate_frame, (0,0), (np.size(annotate_frame,1),30), [0,0,0], -1, cv2.CV_AA)
    # Draw video overlays for fish info       
    drawFish(annotate_frame, fish_state)
    coord_text = 'Fish Coords: (' + str(int(fish_x))+ ', ' + str(int(fish_y)) + ')'
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(annotate_frame, coord_text, (1000,20), font, 0.5, [0,0,255], 1, cv2.CV_AA)      
    # Draw video overlays for area info
    cv2.rectangle(annotate_frame,(arena_left,arena_top), (arena_right,arena_bottom), [255,0,0], 2)
    cv2.line(annotate_frame,(arena_centre_row,arena_top), (arena_centre_row,arena_bottom), [255,0,0], 2)
    # Draw frame number
    cv2.putText(annotate_frame, 'Frame: ' + str(frame_count), (10,20), font, 0.5, [0,0,255], 1, cv2.CV_AA)
    
    half_count_text = 'Frames in each half (black, white, unknown): (' + str(frames_black) + ', ' + str(frames_white) + ', ' + str(frames_unknown) + ')'
    cv2.putText(annotate_frame, half_count_text, (400,20), font, 0.5, [0,0,255], 1, cv2.CV_AA)    
    
    cv2.putText(annotate_frame, 'User clicks: ' + str(user_clicks), (200,20), font, 0.5, [0,0,255], 1, cv2.CV_AA)    
    
    drawParticles(annotate_frame)    
    return annotate_frame
    

def tallyArenaHalf(fish_x, arena_coords):
    """Count the number of frames the fish spent in each half.
    """
    global frames_black
    global frames_white
    global frames_unknown
    (arena_top, arena_bottom, arena_left, arena_right, arena_centre_row) = arena_coords
    
    if fish_x == -100:
        frames_unknown += 1  
    elif fish_x<arena_left:
        frames_unknown+=1
    elif fish_x<arena_centre_row:
        frames_black+=1
    elif fish_x>arena_centre_row:
        frames_white+=1
    return

    

#fourcc = cv2.cv.CV_FOURCC(*'MPG4')
fourcc = cv2.cv.CV_FOURCC(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 30.0, (1280,720))

cv2.namedWindow('video')
cv2.setMouseCallback('video', selectFish)
frame_count = 0
while(cap.isOpened()):
    ret, raw_frame = cap.read()
    if not ret:
        break
    frame_count+=1

    # Run scene processing algorithms
    arena_coords = detectArena(raw_frame)
    (observe_map, filtered) = detectFish(raw_frame, fish_hist_black, fish_hist_white, arena_coords)
    fish_state = trackFish(observe_map, arena_coords, filtered)
    tallyArenaHalf(fish_state[0], arena_coords)
    
    # Display frame with relevant info overlaid
    annotate_frame = annotateFrame(raw_frame, fish_state, arena_coords)
    cv2.imshow('video', annotate_frame)
    out.write(annotate_frame)  # Write frame to output video
    
    # Process user key presses
    key_pressed = cv2.waitKey(1)   
    if key_pressed & 0xFF == ord('q'):
        break
    elif key_pressed & 0xFF == ord(' '):
        video_paused = True
        while(video_paused):  # Pause video until mouse is clicked or space is pressed
            key_pressed = cv2.waitKey(1)
            if key_pressed & 0xFF == ord(' '):
                video_paused = False
    
# Clean up
cap.release()
out.release()
cv2.destroyAllWindows()














