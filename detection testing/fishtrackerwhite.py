# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 18:40:23 2015

@author: steven leigh
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt


cap = cv2.VideoCapture('Video03_09Nov2015.mp4')

while(cap.isOpened()):
    ret, frame = cap.read()

    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    HSV = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hue = HSV[:,:,0]
    sat = HSV[:,:,1]
    val = HSV[:,:,2]
    
    #hue = (hue<65) & (hue>0)
    hue = (hue + 180) % 360    
    #hue = (hue<200) & (hue>170)
    hue = (hue<200) & (hue>50)
    #hue = hue<20
    sat = (sat>15)
    val = (val>20)
    
    #combine = hue / 360.0
    combine = (hue & sat & val) * 1.0
    #combine = sat * 1.0
    #combine = hue * 1.0
    #combine = val * 1.0
    
    cv2.imshow('combined frame', combine)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break