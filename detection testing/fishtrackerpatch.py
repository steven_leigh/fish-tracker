# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 18:40:23 2015

@author: steven leigh
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt


cap = cv2.VideoCapture('Video03_09Nov2015.mp4')
frame = None
fish_hist = None


#Detection algorithm tuning parameters
HIST_CHANNELS = [0,1]
HIST_BINS = [64, 64]
HIST_RANGES = [0,180,0,256]


def calcHist(x,y):
    global frame
    global fish_hist
    mask = np.zeros([np.size(frame,0), np.size(frame,1)], np.uint8)
    cv2.circle(mask, (x,y), 10, 1)
    fish_hist = cv2.calcHist([frame], HIST_CHANNELS, mask, HIST_BINS, HIST_RANGES)
    patch = frame[y-10:y+10,x-10:x+10,:]
    
    cv2.imshow('patch',patch)
    

def selectFish(event, x, y, flags, param):
    """Even triggered when user left clicks on fish.
    """
    global video_paused       
    if event != cv2.EVENT_LBUTTONUP:
        return
    calcHist(x,y)        
    video_paused = False
    

def histFilt(frame):
    global fish_hist
    if fish_hist is None:
        return None
    WINDOW_SIZE=11
    observe = np.zeros((np.size(frame,0), np.size(frame,1)))
    cv2.startWindowThread()
    for x in range(WINDOW_SIZE,np.size(frame,0)-WINDOW_SIZE):
        print x
        for y in range(WINDOW_SIZE,np.size(frame,1)-WINDOW_SIZE):
            patch = frame[x-WINDOW_SIZE:x+WINDOW_SIZE, y-WINDOW_SIZE:y+WINDOW_SIZE]
            hist = cv2.calcHist([patch], HIST_CHANNELS, None, HIST_BINS, HIST_RANGES)
            similarity = cv2.compareHist(hist, fish_hist, cv2.cv.CV_COMP_CORREL)
            observe[x,y] = similarity
    
    observe *= 1.0/np.max(observe)
    #observe -= 4.0
    for r in range(np.size(observe,0)):
        for c in range(np.size(observe,1)):
            if observe[r,c]<0:
                observe[r,c]=0
    cv2.namedWindow('observe')   
    cv2.imshow('observe', observe)     
    cv2.startWindowThread()
    return observe
            
            
    
    

cv2.namedWindow('video')
cv2.setMouseCallback('video', selectFish)

while(cap.isOpened()):   
    ret, frame = cap.read()  
    
    cv2.imshow('video', frame)
    
    histFilt(frame)    
    
    #process user key presses
    key_pressed = cv2.waitKey(1)   
    if key_pressed & 0xFF == ord('q'):
        break
    elif key_pressed & 0xFF == ord(' '):
        video_paused = True
        while(video_paused):  #pause video until mouse is clicked or space is pressed
            key_pressed = cv2.waitKey(1)
            if key_pressed & 0xFF == ord(' '):
                video_paused = False