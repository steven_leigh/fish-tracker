# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 18:40:23 2015

@author: steven leigh
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt


cap = cv2.VideoCapture('Video03_09Nov2015.mp4')

while(cap.isOpened()):
    ret, frame = cap.read()

    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    edge = cv2.Laplacian(frame,8, ksize=3)

    
    cv2.imshow('combined frame', edge)
    cv2.imshow('input', frame)
    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        break
    elif key & 0xFF == ord('p'):
        while True:
            key = cv2.waitKey(1)
            if key & 0xFF == ord('p'):
                break